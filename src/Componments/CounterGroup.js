import Counter from "./Counter.js"

const CounterGroup = (props) => {
    const counterList = [...Array(props.size).keys()]
    return (
        <div>
            {counterList.map(key => ((<Counter key = {key}></Counter>)))}
        </div>
    )
}

export default CounterGroup