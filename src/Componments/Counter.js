import {useState} from "react"
const Counter = () =>{

    const [count,setCount] = useState(0)

    const Plus = (count) => {
        setCount(count => count + 1)
    }

    const Reduce = (count) => {
        setCount(count => count - 1)
    }

    return (
        <div>
            <button onClick={Plus}>+</button>
            <span>{count}</span>
            <button onClick={Reduce}>-</button>
        </div>
    )
}

export default Counter