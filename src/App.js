import './App.css';
import MultipleCounter from "./Componments/MultipleCounter"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MultipleCounter></MultipleCounter>
      </header>
    </div>
  );
}

export default App;
