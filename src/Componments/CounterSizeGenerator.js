const CounterSizeGenerator = (props) => {
    const handlerUpdateCount = (event) => {
        props.onChange((Number(event.target.value)))
    }
    return (
        <div>
            size: <input onChange={handlerUpdateCount} value={props.countSize}></input>
        </div>
    )
}

export default CounterSizeGenerator