import CounterGroup from "./CounterGroup"
import { useState } from 'react';
import CounterSizeGenerator from "./CounterSizeGenerator"

const MultipleCounter = () => {
    const [countSize, setCountSize] = useState(0)
    return (
        <div>
            <CounterSizeGenerator onChange={setCountSize} size={countSize}></CounterSizeGenerator>
            <CounterGroup size={countSize}></CounterGroup>
        </div>
    )
}

export default MultipleCounter